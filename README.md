[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

MyFrama est une instance multi-utilisateur de [Shaarli](https://github.com/shaarli/Shaarli/).

Elle contient un template Bootstrap (dans `shaarli/tpl/frama`), une page d'accueil (`index.php`, `layout.php` et `setconfig.php`) pour créer des comptes ainsi que les plugins :

 * `i18n` pour permettre la traduction de Shaarli et ses plugins dans différentes langues
 * `myframa` qui sert de glue javascript entre l'accueil et Shaarli (vérrouiller les plugins obligatoires, personnaliser les liens par défaut, adapter au fait d'être en mode multi-utilisateur…)
 * `tags_advanced` pour tagger automatiquement des liens selon l'url et épingler les tags les plus fréquement utilisés
 * `recovery` pour mémoriser une adresse email dans la configuration et permettre de récupérer un accès à son compte
 * `framanav` pour ajouter la [barre de navigation de Framasoft](https://framagit.org/framasoft/framanav)

Chaque compte utilisateur se trouve dans le dossier `/u/`.

Il s'agit à chaque fois d'une instance autonome qui se crée depuis le formulaire en page d'accueil.

Les principaux dossiers de Shaarli sont des liens symboliques relatifs qui pointent vers `/shaarli/`.

Seuls les dossiers `cache`, `data`, `pagecache`, `tmp` appartiennent réellement à l'utilisateur.

Il ne faut pas tenter une installation depuis le dossier `/shaarli/`.




