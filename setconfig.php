<?php
//-- From shaarli/index.php --------------------------------------------

// Data subdirectory
$GLOBALS['config']['DATADIR'] = 'data';

// Main configuration file
$GLOBALS['config']['CONFIG_FILE'] = $GLOBALS['config']['DATADIR'].'/config.php';

// Link datastore
$GLOBALS['config']['DATASTORE'] = $GLOBALS['config']['DATADIR'].'/datastore.php';

// Banned IPs
$GLOBALS['config']['IPBANS_FILENAME'] = $GLOBALS['config']['DATADIR'].'/ipbans.php';

// Processed updates file.
$GLOBALS['config']['UPDATES_FILE'] = $GLOBALS['config']['DATADIR'].'/updates.txt';

// Access log
$GLOBALS['config']['LOG_FILE'] = $GLOBALS['config']['DATADIR'].'/log.txt';

// For updates check of Shaarli
$GLOBALS['config']['UPDATECHECK_FILENAME'] = $GLOBALS['config']['DATADIR'].'/lastupdatecheck.txt';

// Set ENABLE_UPDATECHECK to disabled by default.
$GLOBALS['config']['ENABLE_UPDATECHECK'] = false;

// RainTPL cache directory (keep the trailing slash!)
$GLOBALS['config']['RAINTPL_TMP'] = 'tmp/';
// Raintpl template directory (keep the trailing slash!)
$GLOBALS['config']['RAINTPL_TPL'] = 'tpl/frama/';

// Thumbnail cache directory
$GLOBALS['config']['CACHEDIR'] = 'cache';

// Atom & RSS feed cache directory
$GLOBALS['config']['PAGECACHE'] = 'pagecache';

/*
 * Global configuration
 */
// Ban IP after this many failures
$GLOBALS['config']['BAN_AFTER'] = 4;
// Ban duration for IP address after login failures (in seconds)
$GLOBALS['config']['BAN_DURATION'] = 1800;

// Feed options
// Enable RSS permalinks by default.
// This corresponds to the default behavior of shaarli before this was added as an option.
$GLOBALS['config']['ENABLE_RSS_PERMALINKS'] = true;
// If true, an extra "ATOM feed" button will be displayed in the toolbar
$GLOBALS['config']['SHOW_ATOM'] = false;

// Link display options
$GLOBALS['config']['HIDE_PUBLIC_LINKS'] = false;
$GLOBALS['config']['HIDE_TIMESTAMPS'] = false;
$GLOBALS['config']['LINKS_PER_PAGE'] = 20;

// Open Shaarli (true): anyone can add/edit/delete links without having to login
$GLOBALS['config']['OPEN_SHAARLI'] = false;

// Thumbnails
// Display thumbnails in links
$GLOBALS['config']['ENABLE_THUMBNAILS'] = true;
// Store thumbnails in a local cache
$GLOBALS['config']['ENABLE_LOCALCACHE'] = true;

// Update check frequency for Shaarli. 86400 seconds=24 hours
$GLOBALS['config']['UPDATECHECK_BRANCH'] = 'stable';
$GLOBALS['config']['UPDATECHECK_INTERVAL'] = 86400;


/*
 * Plugin configuration
 *
 * Warning: order matters!
 *
 * These settings may be be overriden in:
 *  - data/config.php
 *  - each plugin's configuration file
 */
//$GLOBALS['config']['ENABLED_PLUGINS'] = array(
//    'qrcode', 'archiveorg', 'readityourself', 'demo_plugin', 'playvideos',
//    'wallabag', 'markdown', 'addlink_toolbar',
//);
$GLOBALS['config']['ENABLED_PLUGINS'] = array('qrcode', 'addlink_toolbar', 'markdown'); //'framanav'

// Initialize plugin parameters array.
$GLOBALS['plugins'] = array();

// PubSubHubbub support. Put an empty string to disable, or put your hub url here to enable.
$GLOBALS['config']['PUBSUBHUB_URL'] = '';

if (empty($GLOBALS['redirector'])) $GLOBALS['redirector']='';
if (empty($GLOBALS['disablesessionprotection'])) $GLOBALS['disablesessionprotection']=false;
if (empty($GLOBALS['privateLinkByDefault'])) $GLOBALS['privateLinkByDefault']=false;
if (empty($GLOBALS['titleLink'])) $GLOBALS['titleLink']='?';


//-- From shaarli/application/Config.php -------------------------------
// $config['config']['CONFIG_FILE'] replace by $config_file

/**
 * Re-write configuration file according to given array.
 * Requires mandatory fields listed in $MANDATORY_FIELDS.
 *
 * @param array $config     contains all configuration fields.
 * @param bool  $isLoggedIn true if user is logged in.
 *
 * @return void
 *
 * @throws MissingFieldConfigException: a mandatory field has not been provided in $config.
 * @throws UnauthorizedConfigException: user is not authorize to change configuration.
 * @throws Exception: an error occured while writing the new config file.
 */
function writeConfig($config, $isLoggedIn, $config_file)
{
    // These fields are required in configuration.
    $MANDATORY_FIELDS = array(
        'login', 'hash', 'salt', 'timezone', 'title', 'titleLink',
        'redirector', 'disablesessionprotection', 'privateLinkByDefault'
    );

    if (!isset($config_file)) {
        throw new MissingFieldConfigException('CONFIG_FILE');
    }

    // Only logged in user can alter config.
    if (is_file($config_file) && !$isLoggedIn) {
        throw new UnauthorizedConfigException();
    }

    // Check that all mandatory fields are provided in $config.
    foreach ($MANDATORY_FIELDS as $field) {
        if (!isset($config[$field])) {
            throw new MissingFieldConfigException($field);
        }
    }

    $configStr = '<?php '. PHP_EOL;
    $configStr .= '$GLOBALS[\'login\'] = '.var_export($config['login'], true).';'. PHP_EOL;
    $configStr .= '$GLOBALS[\'hash\'] = '.var_export($config['hash'], true).';'. PHP_EOL;
    $configStr .= '$GLOBALS[\'salt\'] = '.var_export($config['salt'], true).'; '. PHP_EOL;
    $configStr .= '$GLOBALS[\'timezone\'] = '.var_export($config['timezone'], true).';'. PHP_EOL;
    $configStr .= 'date_default_timezone_set('.var_export($config['timezone'], true).');'. PHP_EOL;
    $configStr .= '$GLOBALS[\'title\'] = '.var_export($config['title'], true).';'. PHP_EOL;
    $configStr .= '$GLOBALS[\'titleLink\'] = '.var_export($config['titleLink'], true).'; '. PHP_EOL;
    $configStr .= '$GLOBALS[\'redirector\'] = '.var_export($config['redirector'], true).'; '. PHP_EOL;
    $configStr .= '$GLOBALS[\'disablesessionprotection\'] = '.var_export($config['disablesessionprotection'], true).'; '. PHP_EOL;
    $configStr .= '$GLOBALS[\'privateLinkByDefault\'] = '.var_export($config['privateLinkByDefault'], true).'; '. PHP_EOL;

    // Store all $config['config']
    foreach ($config['config'] as $key => $value) {
        $configStr .= '$GLOBALS[\'config\'][\''. $key .'\'] = '.var_export($config['config'][$key], true).';'. PHP_EOL;
    }

    if (isset($config['plugins'])) {
        foreach ($config['plugins'] as $key => $value) {
            $configStr .= '$GLOBALS[\'plugins\'][\''. $key .'\'] = '.var_export($config['plugins'][$key], true).';'. PHP_EOL;
        }
    }

    if (!file_put_contents($config_file, $configStr)
        || strcmp(file_get_contents($config_file), $configStr) != 0
    ) {
        throw new Exception(
            'Shaarli could not create the config file.
            Please make sure Shaarli has the right to write in the folder is it installed in.'
        );
    }
}

?>